package com.ncr.data.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ncr.data.model.persistence.TransactionHeader;
import com.ncr.data.repositories.TransactionHeaderRepository;
import com.ncr.data.model.client.RemoteSapOperationResponse;
import com.ncr.data.model.remote.RemoteSapOperationRequest;
import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Date;

@Service
public class SapService implements RemoteService {
    private static final Logger log = LoggerFactory.getLogger(SapService.class);
    private RestTemplate sapRestTemplate;
    private TransactionHeaderRepository transactionHeaderRepository;
    private TransactionHeader transactionHeader;
    private ObjectMapper mapper = new ObjectMapper();

    @Value("${loyalty-web-services.authentication.username}")
    private String username;
    @Value("${loyalty-web-services.authentication.password}")
    private String password;

    @Autowired
    public SapService(RestTemplate sapRestTemplate, TransactionHeader transactionHeader) {
        this.sapRestTemplate = sapRestTemplate;
        this.transactionHeader = transactionHeader;
    }


    private HttpEntity<TransactionHeader> buildEntity(TransactionHeader request) {
        String auth = username + ":" + password;
        byte[] encodedAuth = Base64.encodeBase64(
                auth.getBytes(Charset.forName("US-ASCII")));

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + new String(encodedAuth));
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        headers.add("X-Requested-With", MediaType.APPLICATION_JSON_VALUE);

        return new HttpEntity<>(request, headers);
    }

    @Override
    public RemoteSapOperationResponse remoteOperation(TransactionHeader request, String url) throws Exception {
        log.info("Enter. Request: " + request.toString());
        RemoteSapOperationResponse response = null;

        try {
            HttpEntity<TransactionHeader> entity = buildEntity(request);

            response = sapRestTemplate.postForObject(url, entity, RemoteSapOperationResponse.class);
            log.info("dopo post. response.code: " + response.getCode() + " - response.description: " + response.getDescription());
        } catch (Exception e) {

            if (response == null) {
                response = new RemoteSapOperationResponse();
            }

            if (e instanceof HttpServerErrorException) {
                response.setCode(((HttpServerErrorException) e).getStatusCode().value());
                response.setDescription(((HttpServerErrorException) e).getMessage());
                log.error(response.toString());
            }

            if (e instanceof ResourceAccessException){
                //response.setCode(((ResourceAccessException) e).getStatusCode().value());
                response.setCode(99);
                response.setDescription(((ResourceAccessException) e).getMessage());
                log.error(response.toString());

            }

            log.info("Error: ", e);
            if (e.getCause() != null && (e.getCause() instanceof IOException)) {
            }
        }

        log.info("Exit. Response: " + response.toString());

        return response;
    }
}
