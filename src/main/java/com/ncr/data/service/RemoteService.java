package com.ncr.data.service;

import com.ncr.data.model.client.RemoteSapOperationResponse;
import com.ncr.data.model.persistence.TransactionHeader;

public interface RemoteService {
   // public RemoteSapOperationResponse remoteOperation(RemoteSapOperationRequest request, String type, String url, int id) throws  Exception;
    public RemoteSapOperationResponse remoteOperation(TransactionHeader request, String url) throws  Exception;
}
