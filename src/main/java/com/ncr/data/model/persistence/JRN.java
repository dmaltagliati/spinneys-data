package com.ncr.data.model.persistence;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property="id")
//@JsonIgnoreProperties({"sequence"})

@Entity
@Table(name="transaction_jrn")
public class JRN {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private int sequence;
    private String line;

    @ManyToOne
    @JoinColumn(name = "transactionHeader_id")
    //@JsonIgnore
    private TransactionHeader transactionHeader;

    public JRN(){}


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLine() {
        return line;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public TransactionHeader getTransactionHeader() {
        return transactionHeader;
    }

    public void setTransactionHeader(TransactionHeader transactionHeader) {
        this.transactionHeader = transactionHeader;
    }
}
