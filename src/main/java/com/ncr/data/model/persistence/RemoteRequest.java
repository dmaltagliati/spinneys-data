package com.ncr.data.model.persistence;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "remote_request")
public class RemoteRequest {
    public static final String STATUS_SUCCESS = "SUCCESS";
    public static final String STATUS_TO_SEND = "TO_SEND";

    private int id;
    //private String url;
    //private String type;


    //@Column(name = "body", columnDefinition = "NVARCHAR(MAX)")
    private String body;
    private String response;
    private String status;
    private Date timestamp;

    @Column(name = "rcpt_num")
    private int receipt;
    //private String incoming;

    public RemoteRequest() {}

    public RemoteRequest(String response, String status, Date timestamp, String body) {
        this.response = response;
        this.status = status;
        this.timestamp = timestamp;
        this.body = body;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public int getReceipt() {
        return receipt;
    }

    public void setReceipt(int receipt) {
        this.receipt = receipt;
    }
//    public String getUrl() {
//        return url;
//    }
//
//    public void setUrl(String url) {
//        this.url = url;
//    }
//
//    public String getType() {
//        return type;
//    }
//
//    public void setType(String type) {
//        this.type = type;
//    }
//
//    public String getBody() {
//        return body;
//    }
//
//    public void setBody(String body) {
//        this.body = body;
//    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

//    public String getIncoming() {
//        return incoming;
//    }
//
//    public void setIncoming(String incoming) {
//        this.incoming = incoming;
//    }

    @Override
    public String toString() {
        return "RemoteRequest{" +
                "id=" + id +
//                ", url='" + url + '\'' +
//                ", type='" + type + '\'' +
//                ", body='" + body + '\'' +
                ", response='" + response + '\'' +
                ", status='" + status + '\'' +
                ", timestamp=" + timestamp +
//                ", incoming='" + incoming + '\'' +
                '}';
    }
}
