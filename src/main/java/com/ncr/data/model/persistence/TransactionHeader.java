package com.ncr.data.model.persistence;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.stereotype.Component;

@Component
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property="id")

@Entity
public class TransactionHeader {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "STORE")
    private String store; //Branch/POS ID of the store or with default value 01

    @Column(name = "TERMINAL")
    private String terminal; //Terminal ID of the POS till or with default value 01

    @Column(name = "TRANS")
    private int receipt; //Is the receipt number of this transaction

    @Column(name = "DATE")
    private String receiptDate; //Receipt date in YYYYMMDD format

    @JsonProperty("TIMESTAMP")
    @Column(name = "TIME_STAMP")
    //private String receiptTime; //Receipt time in HHMMSS, 24-hour System Time
    private String receiptTime; //Receipt time in HHMMSS, 24-hour System Time

    @Column(name = "TOTAL")
    private long amount; //Is the receipt total amount

    @Column(name = "CUSTOMER")
    private String customer;

    @Column(name = "EOD")
    private String eod;

    @Column(name = "FILENAME")
    private String fileName;

    private String status;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "transactionHeader", orphanRemoval=true, cascade = CascadeType.ALL)
    @Column(name = "idcList")
    private List<IDC> idc = new ArrayList<>();

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "transactionHeader", orphanRemoval=true, cascade = CascadeType.ALL)
    //@OneToMany(mappedBy = "transactionHeader", orphanRemoval=true, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @Column(name = "jrnList")
    private List<JRN> jrn = new ArrayList<>();

    public TransactionHeader() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public int getReceipt() {
        return receipt;
    }

    public void setReceipt(int receipt) {
        this.receipt = receipt;
    }

    public String getReceiptDate() {
        return receiptDate;
    }

    public void setReceiptDate(String receiptDate) {
        this.receiptDate = receiptDate;
    }

    public String getReceiptTime() {
        return receiptTime;
    }

    public void setReceiptTime(String receiptTime) {
        this.receiptTime = receiptTime;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public List<IDC> getIdc() {
        return idc;
    }

    public void setIdc(List<IDC> idc) {
        this.idc = idc;
    }

    public List<JRN> getJrn() {
        return jrn;
    }

    public void setJrn(List<JRN> jrn) {
        this.jrn = jrn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getEod() {
        return eod;
    }

    public void setEod(String eod) {
        this.eod = eod;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public String toString(){
        return  "\n id         : " + id +
                "\n store      : " + store +
                "\n terminal   : " + terminal +
                "\n receipt    : " + receipt +
                "\n receiptDate: " + receiptDate +
                "\n receiptTime: " + receiptTime +
                "\n amount     : " + amount;
    }

}
