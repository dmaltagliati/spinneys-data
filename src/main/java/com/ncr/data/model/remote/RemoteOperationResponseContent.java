package com.ncr.data.model.remote;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

//@JsonIgnoreProperties(ignoreUnknown = true)
public class RemoteOperationResponseContent {
    private String success;

    public RemoteOperationResponseContent() {}

    public RemoteOperationResponseContent( String success) {
        this.success = success;
    }


    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    @Override
    public String toString() {
        return "RemoteOperationResponseContent{" +
                ", success='" + success + '\'' +
                '}';
    }
}
