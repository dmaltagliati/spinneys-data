package com.ncr.data.model.client;

import com.ncr.data.model.remote.*;

public class RemoteSapOperationResponse {
    public static final int OK = 0;

    public static final int GENERIC_ERROR = 999;
    private int code;
    private String description = "";

    public RemoteSapOperationResponse() {
        this.code = OK;
    }

    public RemoteSapOperationResponse(int code) {
        this.code = code;
    }

    public RemoteSapOperationResponse(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public RemoteSapOperationResponse(RemoteSapOperationRequest request, RemoteOperationResponseContent content) {

        if ("1".equals(content.getSuccess())) {
            this.code = OK;
        } else {
            this.code = GENERIC_ERROR;
        }
    }


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString(){
        return "code: " + code + " - description: " + description;
    }
}
