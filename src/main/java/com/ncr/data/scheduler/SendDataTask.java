package com.ncr.data.scheduler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ncr.data.model.client.RemoteSapOperationResponse;
import com.ncr.data.model.persistence.RemoteRequest;
import com.ncr.data.model.persistence.TransactionHeader;
import com.ncr.data.repositories.RemoteRequestRepository;
import com.ncr.data.repositories.TransactionHeaderRepository;
import com.ncr.data.service.SapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;


@Component
public class SendDataTask {
    private static final Logger log = LoggerFactory.getLogger(SendDataTask.class);
    private TransactionHeaderRepository transactionHeaderRepository;
    private RemoteRequestRepository remoteRequestRepository;
    private ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private SapService remoteService;

    @Value("${send-data.ws.url}")
    private String sendDataUrl;

    @Autowired
    public SendDataTask(TransactionHeaderRepository transactionHeaderRepository, RemoteRequestRepository remoteRequestRepository){
        this.transactionHeaderRepository = transactionHeaderRepository;
        this.remoteRequestRepository = remoteRequestRepository;
    }

    @Scheduled(cron = "${sendData-task.cron.expression}")
    public void scheduledSendDataTask() {
        log.info("Enter");
        processData();
        log.info("Exit");
    }

    private void processData() {
        RemoteSapOperationResponse response;
        TransactionHeader transactionHeader = new TransactionHeader();

        List<RemoteRequest> dataRequests = remoteRequestRepository.findByStatusOrderByTimestampAsc(RemoteRequest.STATUS_TO_SEND);

        for (RemoteRequest request : dataRequests) {
            log.info("request.getStatus() : " + request.getStatus());
            if (request.getStatus().equals(RemoteRequest.STATUS_TO_SEND)){
                //transactionHeader = transactionHeaderRepository.findByreceipt(request.getReceipt()); //CGA-TABLEJRN#D
                //CGA-TABLEJRN#A BEG
                List<TransactionHeader> lstTrHdr = transactionHeaderRepository.findByreceipt(request.getReceipt());

                if (lstTrHdr.size() > 0) {
                    transactionHeader = lstTrHdr.get(0);
                }
                //CGA-TABLEJRN#A END

                try {
                    log.info("richiesta webfront: " + mapper.writeValueAsString(transactionHeader));
                    response = remoteService.remoteOperation(transactionHeader, sendDataUrl);
                    if (response != null && response.getCode() == 0){ //OK
                        transactionHeader.setStatus(RemoteRequest.STATUS_SUCCESS);
                        transactionHeaderRepository.save(transactionHeader);
                        //String body = mapper.writeValueAsString(transactionHeader);
                        //request.setBody(body);
                        request.setTimestamp(new Date(System.currentTimeMillis()));
                        request.setStatus(RemoteRequest.STATUS_SUCCESS);
                        request.setResponse(response.getDescription());
                        remoteRequestRepository.save(request);
                    }else{
                        request.setTimestamp(new Date(System.currentTimeMillis()));
                        request.setResponse(response.getDescription());
                        remoteRequestRepository.save(request);
                    }
                    log.info("risposta webfront: " + mapper.writeValueAsString(response));
                }catch (Exception e){
                    log.info("Error: ", e);
                }
            }
        }

        }
}
