package com.ncr.data.scheduler;

import com.ncr.data.model.persistence.IDC;
import com.ncr.data.model.persistence.JRN;
import com.ncr.data.model.persistence.RemoteRequest;
import com.ncr.data.model.persistence.TransactionHeader;
import com.ncr.data.repositories.IDCRepository;
import com.ncr.data.repositories.JRNRepository;
import com.ncr.data.repositories.RemoteRequestRepository;
import com.ncr.data.repositories.TransactionHeaderRepository;
import com.sun.javafx.binding.StringFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by User on 05/04/2018.
 */

@Component
public class DataCollectParserTask {
    private static final Logger log = LoggerFactory.getLogger(DataCollectParserTask.class);

    private TransactionHeaderRepository transactionRepository;
    private RemoteRequestRepository remoteRequestRepository;
    private IDCRepository idcRepository;
    private JRNRepository jrnRepository;
    private int jrnSequence;


    @Autowired
    public DataCollectParserTask(TransactionHeaderRepository transactionRepository, IDCRepository idcRepository, JRNRepository jrnRepository, RemoteRequestRepository remoteRequestRepository){
        this.transactionRepository = transactionRepository;
        this.idcRepository = idcRepository;
        this.jrnRepository = jrnRepository;
        this.remoteRequestRepository = remoteRequestRepository;
    }


    @Value("${datacollect.path}")
    private String dataCollectPath;

    @Value("${operational.currency.code}")
    String opCurrencyCode="AED";

    @Value("${exchange.rate}")
    int exchRate=1;

    TransactionHeader transactionHeader;
    IDC idcLine;
    JRN jrnLine;

    @Scheduled(cron = "${datacollect-parsing.cron.expression}")
    public void scheduledDataCollectParserTask() {
        log.info("Enter");

        try {
            parseDataCollect();
            parseJournal();
            //saveOnRemoteRequest();

        } catch (Exception e) {
            log.error("ECCEZIONE SU parseDataCollect: " + e.getMessage());
            e.printStackTrace();
        }
        log.info("Exit");
    }

    @Value("${datacollect.parser.offset}")
    String fileOffest;

    @Value("${departments.file}")
    String s_dpt;

    @Value("${s_reg.file}")
    String s_reg;



    @Value("${delete.offset.file}")
    String deleteOffsetFile;

    private void saveOnRemoteRequest(){
        TransactionHeader transactionHeader;
        RemoteRequest remoteRequest;

        List<TransactionHeader> transactionHeaders = transactionRepository.findByStatus(RemoteRequest.STATUS_TO_SEND);

        for (TransactionHeader th : transactionHeaders){
            remoteRequest = remoteRequestRepository.findByreceipt(th.getReceipt());
            if (remoteRequest != null) {
                continue;
            }
            remoteRequest = new RemoteRequest();
            remoteRequest.setReceipt(th.getReceipt());
            remoteRequest.setStatus(RemoteRequest.STATUS_TO_SEND);
            remoteRequest.setTimestamp(new Date(System.currentTimeMillis()));
            remoteRequestRepository.save(remoteRequest);
        }


    }

    private void createOffset(File files[]) {
        File deleteOffset = new File(deleteOffsetFile);
        File offset = new File(fileOffest);

        if (deleteOffset.exists()){
            log.info(" deleteOffsetFile : " + deleteOffsetFile + " found");
            if (offset.exists()) {
                if (offset.delete()){
                    log.info( offset.getName() + "  deleted");
                }
                if (deleteOffset.delete()){
                    log.info( deleteOffset.getName() + "  deleted");
                }
            }
        }


        if (offset.exists()) {
            return ;
        }

        for (File f : files) {
            if (!f.getName().startsWith("S_IDC") && !f.getName().startsWith("S_JRN")) {
                log.info(f.getName() + " is not a data collect nor JRN");
                continue;
            }
            log.info(f.getName() + " ");

            try {
                RandomAccessFile offsetWriter = new RandomAccessFile(offset, "rw");

                offsetWriter.seek(offsetWriter.length());
                offsetWriter.writeBytes(f.getName() + "=0\r\n");
                //offsetWriter.flush();
                offsetWriter.close();
            } catch (Exception e) {
                log.error("errore creazione file offset. e: " + e.getMessage());
            }
        }
    }

    Properties offsetProperties = new Properties();
    private  long readOffset(String idcName) throws  FileNotFoundException, IOException{
        log.info("ENTER readOffset. Looking for offset datacollect: " + idcName);
        int offset=0;
        offsetProperties.load(new FileInputStream(fileOffest));
        offset = Integer.parseInt(offsetProperties.getProperty(idcName));
        log.info("EXIT readOffset. offset: " + offset);
        return offset;
    }

    private void writeOffset(String idcName, long offset) throws FileNotFoundException, IOException{
        log.info("ENTER writeOffset. Looking for offset datacollect: " + idcName);

        offsetProperties.load(new FileInputStream(fileOffest));
        offsetProperties.put(idcName, String.valueOf(offset));
        offsetProperties.store(new FileOutputStream(fileOffest), "update: " + idcName);
        log.info("EXIT writeOffset. offset: " + offset);

    }



    boolean trxWithHeader = false;
    private void parseDataCollect() throws FileNotFoundException, IOException {
        log.info("*** parseDataCollect beg ***");
        File currentDirectory = new File(dataCollectPath);
        File files[] = currentDirectory.listFiles();
        String line;
        long offset;

        Arrays.sort(files);
        log.info("*** list file beg ***");
        for (File f : files) {
            log.info(f.getName());
        }
        log.info("*** list file end ***");

        createOffset(files);


        for (File f : files) {
            if (!f.getName().startsWith("S_IDC")){
                log.info(f.getName() + " is not a data collect.");
                continue;
            }
            log.info(f.getName() + " reading....");
            transactionHeader = new TransactionHeader();
            RandomAccessFile reader = new RandomAccessFile(f.getAbsoluteFile(), "r");
            offset = readOffset(f.getName());
            reader.seek(offset);

            trxWithHeader = false;
            tmpIdc = new ArrayList<>();  //CGA-TABLEJRN#A
            while ((line = reader.readLine())!= null){
                parseLine(line, reader, f);
            }
            //writeOffset(f.getName(), reader.getFilePointer());
        }
        log.info("*** parseDataCollect end ***");
    }

    ArrayList<String> tmpIdc = new ArrayList<>();  //CGA-TABLEJRN#A
    void parseLine(String line, RandomAccessFile reader, File f){
        DCLine dcRecord = new DCLine(line);
        log.info("ENTER parseLine:>" + line + "<");
        idcLine = new IDC();
        String lineFields[] = line.split(":");
        TransactionHeader transactionHeaderOld = new TransactionHeader();
        boolean found = false;
        int steptrans = 0;

        tmpIdc.add(line);  //CGA-TABLEJRN#A

//        if (dcRecord.isAbortTransaction() || !dcRecord.isGrossSalesTransaction()){
//            log.info("record discarded. isAbort: " + dcRecord.isAbortTransaction() + " - isSales: " + dcRecord.isSalesRecord());
//        }

        if (dcRecord.isHeader()) {
            log.info("isHeader");
            trxWithHeader = true;
            transactionHeader = new TransactionHeader();
        }

        if (dcRecord.isFooter()) {
            log.info("isFooter beg");

            String  receiptDateFormat = dcRecord.getDate().substring(0,4) +
                    "-" + dcRecord.getDate().substring(4,6) +
                    "-" + dcRecord.getDate().substring(6,8);

            log.info("receiptDateFormat: " + receiptDateFormat);

            //CGA-TABLEJRN#A BEG
            if (!trxWithHeader){
                transactionHeader = new TransactionHeader();
            }

            log.info("trxWithHeader: " + trxWithHeader);
            log.info("dcRecord.getTransactionNumber(): " + dcRecord.getTransactionNumber());
            List<TransactionHeader> lstTransHeader = transactionRepository.findByreceipt(dcRecord.getTransactionNumber());
            log.info("lstTransHeader.size(): " + lstTransHeader.size());


            for (TransactionHeader tr : lstTransHeader) {
                log.info("tr.getTerminal(): " + tr.getTerminal());
                log.info("tr.getReceiptDate(): " + tr.getReceiptDate());

                if (Integer.parseInt(tr.getTerminal()) == Integer.parseInt(dcRecord.getTerminalID()) && tr.getReceiptDate().startsWith(receiptDateFormat)) {
                    transactionHeader = new TransactionHeader();
                    transactionHeader = tr;
                    found = true;
                    log.info("transaction found");

                    break;
                }
            }
            //CGA-TABLEJRN#A END


            if (!found) {  //CGA-TABLEJRN#A
                transactionHeader.setReceipt(dcRecord.getTransactionNumber());
                transactionHeader.setTerminal(dcRecord.getTerminalID());
                transactionHeader.setReceiptDate(receiptDateFormat);
                transactionHeader.setStore(dcRecord.getStoreCode());

                //DateFormat receiptDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                //transactionHeader.setReceiptDate(dcRecord.getDate());
                //transactionHeader.setReceiptTime(dcRecord.getTime());
            } else {
                //transactionHeader.setIdc(transactionHeaderOld.getIdc());
                transactionHeader.setStatus(RemoteRequest.STATUS_TO_SEND);
            }

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date();
            System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43

            transactionHeader.setReceiptTime(dateFormat.format(date));
            transactionHeader.setAmount(dcRecord.getAmount());
            transactionHeader.setCustomer(dcRecord.getCustomerCode());

            log.info("transactionHeader : " + transactionHeader.toString());
            log.info("isFooter end");
        }


        //CGA-TABLEJRN#A BEG
        /*steptrans = Integer.parseInt(lineFields[5]);
        idcLine.setSequence(steptrans);
        idcLine.setLine(line);
        idcLine.setTransactionHeader(transactionHeader);
        transactionHeader.getIdc().add(idcLine);
        */
        //CGA-TABLEJRN#A END

        if (dcRecord.isFooter()) {
            log.info("saving transactionHeader");
            //transactionRepository.save(transactionHeader);
            trxWithHeader = false;

            steptrans = 1;
            for (String rowIdc : tmpIdc) {
                idcLine = new IDC();
                idcLine.setSequence(steptrans);
                idcLine.setLine(rowIdc);
                idcLine.setTransactionHeader(transactionHeader);
                transactionHeader.getIdc().add(idcLine);
                log.info("rowIdc: " + rowIdc);

                steptrans++;
            }

            try {
                transactionRepository.save(transactionHeader);
                log.info("saved tbl");
                writeOffset(f.getName(), reader.getFilePointer());
            }catch (Exception e){
                log.error("error writing offset of : " + f.getName());
            }
        }

        //CGA-TABLEJRN#A BEG
        if (found) {
            saveOnRemoteRequest();
        }
        //CGA-TABLEJRN#A END
    }

    ArrayList<String> tmpJrn = new ArrayList<>();
    private void parseJournal() throws FileNotFoundException, IOException {
        log.info("*** parseJournal beg ***");
        File currentDirectory = new File(dataCollectPath);
        File files[] = currentDirectory.listFiles();
        String line;
        long offset;

        Arrays.sort(files);
        log.info("*** list file beg ***");
        for (File f : files) {
            log.info(f.getName());
        }
        log.info("*** list file end ***");

        createOffset(files);

        for (File f : files) {
            if (!f.getName().startsWith("S_JRN")){
                log.info(f.getName() + " is not an electronic journal.");
                continue;
            }
            log.info(f.getName() + " reading journal ....");
            transactionHeader = new TransactionHeader();
            RandomAccessFile reader = new RandomAccessFile(f.getAbsoluteFile(), "r");
            offset = readOffset(f.getName());
            reader.seek(offset);

            jrnLine = new JRN();
            tmpJrn = new ArrayList<>();
            while ((line = reader.readLine())!= null){
                transactionHeader = new TransactionHeader();
                parseJRNLine(line, reader, f);
            }
            //writeOffset(f.getName(), reader.getFilePointer());
        }
        log.info("*** parseJournal end ***");
    }

    void parseJRNLine(String line, RandomAccessFile reader, File f){
        log.info("ENTER parseJRNLine:>" + line + "<");
        int rcpt_num = 0;
        String reg = "";
        String store = "";
        String date = "";
        String time = "";
        boolean found = false;

        tmpJrn.add(line);

        if (line.charAt(0) == '*') {
            jrnSequence = 1;

            rcpt_num = Integer.parseInt(line.substring(1, 5));

            //CGA-TABLEJRN#A BEG
            store = line.substring(6, 10);
            reg = line.substring(11, 14);
            date = line.substring(21, 29);  // 13.03.19
            time = line.substring(30, 35);  // 11:37
            String dateFormat = "20" + date.substring(6) + "-" + date.substring(3, 5) + "-" + date.substring(0, 2);

            log.info("reg: " + reg);
            log.info("date >" + date + "<");
            log.info("dateFormat >" + dateFormat + "<");
            //CGA-TABLEJRN#A END

            //transactionHeader = transactionRepository.findByreceipt(rcpt_num);  //CGA-TABLEJRN#D
            List<TransactionHeader> lstTransHeader = transactionRepository.findByreceipt(rcpt_num);  //CGA-TABLEJRN#A
            log.info("lstTransHeader.size(): " + lstTransHeader.size());

            //CGA-TABLEJRN#A BEG

            for (TransactionHeader tr : lstTransHeader) {
                log.info("tr.getTerminal(): " + tr.getTerminal());
                log.info("tr.getReceiptDate(): " + tr.getReceiptDate());

                if (Integer.parseInt(tr.getTerminal()) == Integer.parseInt(reg) && tr.getReceiptDate().startsWith(dateFormat)) {
                    transactionHeader = tr;
                    found = true;
                    break;
                }
            }

            /*if (transactionHeader == null) {
                transactionHeader = new TransactionHeader();
                transactionHeader.setReceipt(rcpt_num);
                log.warn("journal without datacollect. Creating a TransacionHeader void with transaction number: " + rcpt_num);
                return;
            }*/

            log.info("found: " + found);
            if (!found) {
                log.info("hearder not found");
                transactionHeader = new TransactionHeader();

                transactionHeader.setReceipt(rcpt_num);
                log.info("rcpt_num: " + rcpt_num);

                transactionHeader.setTerminal(reg);
                log.info("reg: " + reg);

                transactionHeader.setReceiptDate(dateFormat);
                log.info("data: " + dateFormat);

                transactionHeader.setReceiptTime(dateFormat + " " + time);
                transactionHeader.setStore(store);
                transactionHeader.setAmount(0);
                transactionHeader.setCustomer("");
            } else {
                transactionHeader.setStatus(RemoteRequest.STATUS_TO_SEND);
            }
            //CGA-TABLEJRN#A END

            String tmpLine = "";
            Iterator<String> tmpJrnItr = tmpJrn.iterator();

            log.info("set jrn fields and save in the table");
            while(tmpJrnItr.hasNext()){
                tmpLine = tmpJrnItr.next();
                jrnLine = new JRN();
                jrnLine.setLine(tmpLine);
                jrnLine.setTransactionHeader(transactionHeader);
                jrnLine.setSequence(jrnSequence);
                transactionHeader.getJrn().add(jrnLine);
                //transactionHeader.setStatus(RemoteRequest.STATUS_TO_SEND);  //CGA-TABLEJRN#D
                //transactionRepository.save(transactionHeader);

                log.info("line: " + tmpLine);
                jrnSequence++;
            }

            try {
                transactionRepository.save(transactionHeader);
                writeOffset(f.getName(), reader.getFilePointer());
            }catch (Exception e){
                log.error("error writing offset of : " + f.getName());
            }

            jrnLine = new JRN();
            tmpJrn = new ArrayList<>();
        }

        //CGA-TABLEJRN#A BEG
        if (found) {
            saveOnRemoteRequest();
        }
        //CGA-TABLEJRN#A END
    }
}


class DCLine {
    private static final Logger log = LoggerFactory.getLogger(DCLine.class);
    String dcRecord;
    String[] fields;


    public DCLine(String line) {

        dcRecord = line;
        fields = line.split(":");
        log.info(" fields.length: " + fields.length);
    }


    int getTransactionNumber(){
        log.info(" getTransactionNumber - return fields[4]: >" + fields[4]+"<");
        return Integer.parseInt(fields[4]);
    }

    boolean isSalesRecord(){
        log.info(" isSalesRecord - return fields[6]: >" + fields[6]+"< : " + fields[6].equals("S"));
        return fields[6].equals("S");
    }

    boolean isHeader(){
        log.info(" isHeader return fields[6]: >" + fields[6]+"< : " + fields[6].equals("H"));
        return fields[6].equals("H");
    }

    boolean isFooter(){
        log.info(" isFooter return fields[6]: >" + fields[6]+"< -  : " + fields[6].equals("F"));
        return fields[6].equals("F");
    }

    boolean isVatTax(){
        log.info(" isVatTax - return fields[6]: >" + fields[6]+"< -  : " + (fields[6].equals("V") && fields[7].charAt(2)=='0'));
        return fields[6].equals("V") && fields[7].charAt(2)=='0';
    }

    boolean isTender(){
        log.info(" isTender - return fields[6]: >" + fields[6]+"< -  : " + fields[6].equals("T"));
        boolean isTender = false;
        if (fields[6].equals("T")){
            isTender = true;
            if (Integer.parseInt(fields[10].substring(0,2)) >= 30) { //foreign currency
                if ((fields[7].charAt(2) % 2) == 0){
                    isTender = false;
                }
            }
        }
        return  isTender;
    }

    boolean isItemDiscount(){
        log.info(" isItemDiscount- return fields[6]: >" + fields[6]+"<");
        return fields[6].equals("C");
    }

    String getItemCode(){
        log.info(" getItemCode - return fields[9]: >" + fields[9].substring(0,16).trim()+"<");
        return fields[9].substring(0,16).trim();
    }

    Long getSalesAmount(){
        log.info(" getSalesAmount - return fields[9]: >" + fields[9].substring(26)+"<");
        return Long.parseLong(fields[9].substring(26));
    }

    Long getDiscountAmount(){
        log.info("ENTER getDiscountAmount - return fields[9]: >" + fields[9].substring(25)+"<");
        long value = 0;

        switch(fields[9].charAt(25)){
            case '<':
                value = Long.parseLong(fields[9].substring(26)) * -1;
                break;
            case '>':
                log.info("Long.parseLong(fields[9].substring(26): " + Long.parseLong(fields[9].substring(26)));
                value = Long.parseLong(fields[9].substring(26));
                break;
            default:
                try {
                    value = Long.parseLong(fields[9].substring(25));
                }catch (Exception e){
                    log.error("ECCEZIONE su getDiscountAmount. e:"+e.getMessage());
                    value = 0;
                }
        }

        log.info("EXIT getDiscountAmount. value: " + value);
        return value;
    }

    Long getAmount(){
        log.info(" getAmount - return fields[10]: >" + fields[10].substring(8)+"<");
        return Long.parseLong(fields[10].substring(8));
    }

    boolean isGrossSalesTransaction(){
        log.info(" isGrossSalesTransaction - return fields[7]: >" + fields[7]+"< : " + (fields[7].charAt(0) == '1'));
        return fields[7].charAt(0) == '1';
    }

    boolean isAbortTransaction(){
        log.info(" isAbortTransaction - return fields[7]: >" + fields[7]+"< - " + (fields[7].charAt(0) == '2' || fields[7].charAt(0) == '8'));
        return fields[7].charAt(0) == '2' || fields[7].charAt(0) == '8';
    }

    String getDate(){
        log.info(" getDate - return fields[2]: >" + fields[2]+"<");
        return "20"+fields[2];
    }

    String getTime(){
        log.info(" getTime - return fields[3]: >" + fields[3]+"<");
        return fields[3];
    }

    String getStoreCode(){
        log.info(" getStoreCode - return fields[0]: >" + fields[0]+"<");
        return fields[0];
    }

    String getTerminalID(){
        log.info(" getTerminalID - return fields[1]: >" + fields[1]+"<");
        return fields[1];
    }

    String getCustomerCode(){
        log.info(" getCustomercode - return fields[9]: >" + fields[9]+"<");
        String customerCode = "";

        if (fields[10].startsWith("00")){
            customerCode = fields[9];
        }
        return customerCode;
    }

//    int getItemQty(){
//        log.info(" getItemQty - return fields[9]: >" + fields[9].substring(16, 21)+"<");
//        return Integer.parseInt(fields[9].substring(16, 21));
//    }

    double getItemQty(){
        log.info("ENTEr getItemQty - return fields[9]: >" + fields[9].substring(16, 21)+"<");
        String qtyField = fields[9].substring(16, 25);
        double itemQty;
        if (qtyField.indexOf(".") >= 0){
            itemQty = Double.parseDouble(fields[9].substring(16, 25));
        }else{
            itemQty = Double.parseDouble(fields[9].substring(16, 21));
        }
        log.info("EXIT getItemQty :" + itemQty);
        return itemQty;
    }


    String getStatus(){
        String status = "";

        if (fields[7].charAt(0) == '1')
            status = "SALES";
        if (fields[7].charAt(1) == '4')
            status = "RETURN";

        log.info(" getStatus return status: >" + status+"< - " + fields[7]);
        return  status;
    }

    String getTenderID(){
        log.info(" getTenderID return fields[9]: >" + fields[10].substring(0, 2)+"<");
        return fields[10].substring(0, 2);
    }

    String getDepartment(){
        log.info(" getDepartment - return fields[8]: >" + fields[8]+"<");
        return  fields[8];
    }
}

