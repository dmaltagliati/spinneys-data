package com.ncr.data.scheduler.services;

import java.io.File;

public interface MaintenanceService {
    public void process(File maintenanceFile) throws Exception;
}
