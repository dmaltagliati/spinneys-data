package com.ncr.data.repositories;

import com.ncr.data.model.persistence.TransactionHeader;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TransactionHeaderRepository extends CrudRepository<TransactionHeader, Integer> {
    //TransactionHeader findByreceipt(int RCPT_NUM);  //cga d
    List<TransactionHeader> findByreceipt(int RCPT_NUM); //cga a
    TransactionHeader findById(int id);
    List<TransactionHeader> findByStatus(String status);
}
