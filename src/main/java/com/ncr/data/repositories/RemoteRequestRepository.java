package com.ncr.data.repositories;

import com.ncr.data.model.persistence.RemoteRequest;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RemoteRequestRepository extends CrudRepository<RemoteRequest, Integer> {
    List<RemoteRequest> findByStatusOrderByTimestampAsc(String status);
    RemoteRequest findByreceipt(int rcpt_num);
}
