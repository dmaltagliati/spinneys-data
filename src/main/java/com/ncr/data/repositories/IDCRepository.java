package com.ncr.data.repositories;

import com.ncr.data.model.persistence.IDC;
import org.springframework.data.repository.CrudRepository;

public interface IDCRepository extends CrudRepository<IDC, Integer> {
}
