package com.ncr.data.repositories;

import com.ncr.data.model.persistence.JRN;
import org.springframework.data.repository.CrudRepository;

public interface JRNRepository extends CrudRepository<JRN, Integer> {
}
