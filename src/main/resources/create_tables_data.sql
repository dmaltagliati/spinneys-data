USE [mtx]
GO

/****** Object:  Table [mtxadmin].[remote_request]    Script Date: 01/07/2019 14:53:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [mtxadmin].[remote_request](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[body] [varchar](255) NULL,
	[receipt] [int] NOT NULL,
	[response] [varchar](255) NULL,
	[status] [varchar](255) NULL,
	[timestamp] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



/****** Object:  Table [mtxadmin].[transaction_header]    Script Date: 01/07/2019 14:54:38 ******/


CREATE TABLE [mtxadmin].[transaction_header](
[id] [int] IDENTITY(1,1) NOT NULL,
[total] [bigint] NULL,
[customer] [varchar](255) NULL,
[eod] [varchar](255) NULL,
[filename] [varchar](255) NULL,
[trans] [int] NULL,
[date] [varchar](255) NULL,
[time_stamp] [varchar](255) NULL,
[status] [varchar](255) NULL,
[store] [varchar](255) NULL,
[terminal] [varchar](255) NULL,
PRIMARY KEY CLUSTERED
(
[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


GO


/****** Object:  Table [mtxadmin].[transaction_idc]    Script Date: 01/07/2019 14:55:53 ******/


CREATE TABLE [mtxadmin].[transaction_idc](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[line] [varchar](255) NULL,
	[sequence] [int] NOT NULL,
	[transaction_header_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [mtxadmin].[transaction_idc]  WITH CHECK ADD  CONSTRAINT [FKa67u3uyu3cb9gk62hdpeyu6r7] FOREIGN KEY([transaction_header_id])
REFERENCES [mtxadmin].[transaction_header] ([id])
GO

ALTER TABLE [mtxadmin].[transaction_idc] CHECK CONSTRAINT [FKa67u3uyu3cb9gk62hdpeyu6r7]
GO


/****** Object:  Table [mtxadmin].[transaction_jrn]    Script Date: 01/07/2019 14:56:46 ******/

SET ANSI_PADDING ON
GO

CREATE TABLE [mtxadmin].[transaction_jrn](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[line] [varchar](255) NULL,
	[sequence] [int] NOT NULL,
	[transaction_header_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [mtxadmin].[transaction_jrn]  WITH CHECK ADD  CONSTRAINT [FKf3cqaiv88xgqtn818lakdbt6b] FOREIGN KEY([transaction_header_id])
REFERENCES [mtxadmin].[transaction_header] ([id])
GO

ALTER TABLE [mtxadmin].[transaction_jrn] CHECK CONSTRAINT [FKf3cqaiv88xgqtn818lakdbt6b]
GO


